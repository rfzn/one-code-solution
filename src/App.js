
import { Route, Routes } from 'react-router-dom';
import './App.css';
import LandingPage from './pages/LandingPage/LandingPage';
import Login from './pages/Login/Login';
import Dashboard from './pages/Dashboard/Dashboard';
import Detail from './pages/Detail/Detail';
import Comment from './pages/Comment/Comment';
import DetailUser from './pages/DetailUser/DetailUser';
// import Dashboard2 from './pages/Dashboard/Dashboard2';

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/login" element={<Login />} />
        <Route path="/dashboard" element={<Dashboard />} />
        
        <Route path="/detail-user" element={<DetailUser />} />
        <Route path="/comment" element={<Comment />} />
        <Route path="/detail" element={<Detail />} />
      </Routes>
    </>
  );
}

export default App;
