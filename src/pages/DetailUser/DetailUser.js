import React from 'react'
import { useLocation, useNavigate } from 'react-router-dom'

const DetailUser = () => {
    const location = useLocation()
    const navigate = useNavigate()
    console.log(location);
    const {user} = location.state
  return (
    <div>
        <nav className='flex justify-between px-16 py-20 w-full'>
        <div>
            <h4 className='font-bold text-2xl'>Cinta Koding</h4>
        </div>
        <div>
            <h4 className='font-bold text-gray-400 text-center text-xl border-b-4 border-sky-500 w-20'>Post</h4>
        </div>
        <div>
        <h4 onClick={()=> navigate('/detail-user', {state: {user:user}})} className='font-bold text-2xl'>Welcome, <span className='text-sky-500'>{user.map((item)=> (item.username))}</span></h4>
        </div>
    </nav>
    <div>
    <div className="px-4 my-5">

      <div>
        <button onClick={()=>navigate(-1)} className='w-1/3 flex justify-start mx-auto'>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
          <path strokeLinecap="round" strokeLinejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18" />
        </svg>
        </button>
            {
                user.map((item)=> (
            <table key={item.id} className='mx-auto w-1/2'>
                <tr>
                    <td>Username</td>
                    <td>:</td>
                    <td>{item.username}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>{item.email}</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>:</td>
                    <td>{item.address.city}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>:</td>
                    <td>{item.phone}</td>
                </tr>
            </table>
                ))
            }
            
        
      </div>
      
    </div>
    </div>
    </div>
  )
}

export default DetailUser