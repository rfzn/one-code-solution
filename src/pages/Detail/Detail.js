import axios from 'axios'
import React, { useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'


const Detail = () => {
    const [totalComments, setTotalComments] = useState(0)
    const location = useLocation();
    const { details } = location.state;
    const { user } = location.state;
    const { author } = location.state;
    const { comments } = location.state;
    const { comment } = location.state;
    // const [allComment, setAllComment] = useState(comments)
    
    // console.log(user);
    // console.log(details);
    // console.log(location);
    const navigate =useNavigate()

  return (
    <div>
        <nav className='flex justify-between px-16 py-20 w-full'>
        <div>
            <h4 className='font-bold text-2xl'>Cinta Koding</h4>
        </div>
        <div>
            <h4 className='font-bold text-gray-400 text-center text-xl border-b-4 border-sky-500 w-20'>Post</h4>
        </div>
        <div>
        <h4 onClick={()=> navigate('/detail-user', {state: {user:user}})} className='font-bold text-2xl'>Welcome, <span className='text-sky-500'>{user.map((item)=> (item.username))}</span></h4>
        </div>
    </nav>
    <div>
    <div className="px-4 my-5">
    
      <div>
        <button onClick={()=>navigate(-1)} className='w-1/3 flex justify-start mx-auto'>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
          <path strokeLinecap="round" strokeLinejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18" />
        </svg>
        </button>
          <div className="flex justify-start mx-auto text-left p-5 w-1/3 gap-4 flex-row">
            <div className='text-base'>{author}</div>
            <div className='flex flex-col gap-5 w-full'>
              <div>
                <p className="text-gray-800">{details.title}</p>
              </div>
              <div>
                <p className="text-gray-500">{details.body}</p>
              </div>
              <div className='flex flex-row justify-between w-full text-sky-500'>
                <div className='inline-flex'>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M12 20.25c4.97 0 9-3.694 9-8.25s-4.03-8.25-9-8.25S3 7.444 3 12c0 2.104.859 4.023 2.273 5.48.432.447.74 1.04.586 1.641a4.483 4.483 0 01-.923 1.785A5.969 5.969 0 006 21c1.282 0 2.47-.402 3.445-1.087.81.22 1.668.337 2.555.337z" />
                </svg>
                {comment}
                </div>
                </div>
            </div>
          </div>
        
      </div>
      
    </div>
    </div>
    </div>
  )
}

export default Detail