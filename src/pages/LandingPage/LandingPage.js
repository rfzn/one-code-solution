import React from 'react'
import { Link } from 'react-router-dom'
import { img } from '../../img'
// import Login from '../Login'

const LandingPage = () => {

  return (
    <>
    <div>
        <nav className='flex justify-between py-3 px-8'>
            <div>
                <h4 className='font-bold text-2xl'>Cinta Koding</h4>
            </div>
            <div>
                <Link to='/login' className='px-8 py-2 bg-sky-500 rounded-3xl text-white font-semibold'>Login</Link>
            </div>
        </nav>
    </div>
    <div className=' w-full'>
        <img className=' mt-6 mx-auto w-1/2' src={img} alt="" />
        {/* <img src="" alt="1" /> */}
        {/* <h1>Halaman Pertama</h1> */}
    </div>
    </>
  )
}

export default LandingPage