import axios from 'axios';
import React, { useEffect, useState } from 'react'
import ReactPaginate from 'react-paginate';
import {useLocation, useNavigate } from 'react-router-dom';

const Dashboard = () => {
  const [posts, setPosts] = useState([]);
  const [users, setUsers] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  // const [details, setDetails] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(10);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);
  const navigate = useNavigate()

  const getUserName = (userId) => {
    const user = users.find((user) => user.id === userId);
    return user ? user.username : '';
  };

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const location = useLocation();
  const { data } = location.state;
  const [comments, setComments] = useState();  

  useEffect(() => {
    const fetchPosts = async () => {
      const { data: postsData } = await axios.get(
        'https://jsonplaceholder.typicode.com/posts'
      );
      setPosts(postsData);
    } 
    fetchPosts();
  }, []);

  useEffect(() => {
    const fetchUsers = async () => {
      const { data: usersData } = await axios.get(
        'https://jsonplaceholder.typicode.com/users'
      );
      setUsers(usersData);
    };

    fetchUsers();
  }, []);

  

  useEffect(() => {
    const fetchComments = async () => {
      const { data } = await axios.get(
        "https://jsonplaceholder.typicode.com/comments"
      );
      setComments(data);
    };

    fetchComments();
  }, []);

  const filteredPosts = posts.filter(
    (post) =>
      post.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
      post.body.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
  };

  const fetchDetails = async (post) => {
    const user = getUserName(post.userId)
    const comment = comments.filter(comment => comment.postId === post.id).length
    const response = await axios.get(`https://jsonplaceholder.typicode.com/posts/${post.id}`);
    console.log(response.data);
    // setDetails(response.data);
    navigate('/detail', {state: {details:response.data, user:data, author: user, comment: comment }})
  };

  const handleComments = async (post) => {
    const user = getUserName(post.userId)
    const comment = comments.filter(comment => comment.postId === post.id).length
    const response = await axios.get(`https://jsonplaceholder.typicode.com/posts/${post.id}`);
    console.log(response.data)
    navigate('/comment', {state: {details:response.data, user:data, author: user, comment: comment }})
  }
  // const [searchTerm, setSearchTerm] = useState("");
  const [pageNumber, setPageNumber] = useState(0);

  // const postsPerPage = 10;
  const pagesVisited = pageNumber * postsPerPage;

  const displayPosts = posts
    .filter((post) => {
      if (searchTerm === "") {
        return post;
      } else if (
        post.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
        post.body.toLowerCase().includes(searchTerm.toLowerCase())
      ) {
        return post;
      }
      return null;
    })
    .slice(pagesVisited, pagesVisited + postsPerPage)
    .map((post) => {
      return (
        <div key={post.id} className="flex justify-start mx-auto text-left p-5 w-1/3 gap-4 flex-row">
           <div className='text-base font-bold'>{getUserName(post.userId)}</div>
             <div className='flex flex-col gap-5 w-full'>
               <div>
                 <p className="text-gray-500">{post.title}</p>
               </div>
               <div className='flex flex-row justify-between w-full text-sky-500'>

                 <button onClick={()=>handleComments(post)} className='inline-flex'>
                 <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                   <path strokeLinecap="round" strokeLinejoin="round" d="M12 20.25c4.97 0 9-3.694 9-8.25s-4.03-8.25-9-8.25S3 7.444 3 12c0 2.104.859 4.023 2.273 5.48.432.447.74 1.04.586 1.641a4.483 4.483 0 01-.923 1.785A5.969 5.969 0 006 21c1.282 0 2.47-.402 3.445-1.087.81.22 1.668.337 2.555.337z" />
                 </svg>
                 {comments?.filter(comment => comment.postId === post.id).length}
                 </button>
                 <div>
                   <button onClick={()=>fetchDetails(post)} >Detail</button>
                 </div>
                 </div>
             </div>
          
         </div>
      );
    });

  const pageCount = Math.ceil(posts.length / postsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  useEffect(() => {
    axios.get("https://jsonplaceholder.typicode.com/posts").then((response) => {
      setPosts(response.data);
    });
  }, []);

  return (
    <div>
      <nav className='flex justify-between px-16 py-20 w-full'>
         <div>
             <h4 className='font-bold text-2xl'>Cinta Koding</h4>
         </div>
         <div>
             <h4 className='font-bold text-gray-400 text-center text-xl border-b-4 border-sky-500 w-20'>Post</h4>
         </div>
         <div>
         <h4 onClick={()=> navigate('/detail-user', {state: {user:data}})} className='font-bold     text-2xl'>Welcome, <span className='text-sky-500'>{data.map((item)=>(item.username))}</span></h4>
         </div>
     </nav>
     <div className='bg-slate-200 w-2/5 flex justify-center mx-auto rounded-3xl px-4 py-2'>
       <input
         className="appearance-none text-center bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
         type="text"
         placeholder="Search"
         onChange={handleSearch}
       />
       <button
         type="button"
       >
         <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
   <path strokeLinecap="round" strokeLinejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
 </svg>
    </button>
      </div>
             
      {displayPosts}
      <ReactPaginate
        previousLabel={"Previous"}
        nextLabel={"Next"}
        pageCount={pageCount}
        onPageChange={changePage}
        containerClassName={"flex justify-center gap-3 my-10"}
        previousLinkClassName={"py-1 px-3"}
        nextLinkClassName={"py-1 px-3"}
        disabledClassName={"opacity-50 cursor-not-allowed"}
        activeClassName={"border-b-2 border-sky-500 text-center w-4"}
      />
    </div>
  );


}
export default Dashboard