import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'

const Login = () => {
  const [username, setUserName] = useState('')
  const [password, setPassword] = useState('')
  const navigate = useNavigate();

  const handleSearch = async () => {
    console.log('fetching');
    try {
      const response = await axios.get(`https://jsonplaceholder.typicode.com/users?username=${username}`);
      const user = response.data[0];
      if (user) {
        navigate('/dashboard', { state: { data: response.data } });
      } else {
        alert('Username or password is incorrect');
      }
    } catch (error) {
      console.error(error);
    }
  };

  const getData = (e) => {
    e.preventDefault();
    if(username === password) {
      handleSearch()
    }else{
      alert('Username or password is incorrect');
    }
  }

  return (
    <div className='flex justify-center items-center h-screen'>
      <form className='flex flex-col justify-center items-center gap-5 w-4/12'>
      <h2 className='text-lg font-semibold'>Login Page</h2>
        <input type="text" className='border-2 py-2 px-8 w-full border-sky-500 rounded-3xl' placeholder='username' onChange={(e)=>setUserName(e.target.value)} required/>
        <input type="password" className='border-2 py-2 px-8 w-full border-sky-500 rounded-3xl' placeholder='password' onChange={(e)=>setPassword(e.target.value)} required/>
        <button onClick={(e)=> getData(e)} className='px-8 py-2 w-full bg-sky-500 rounded-3xl text-white font-semibold'>Login</button>
      </form>
    </div>
  )
}

export default Login